package com.hunt.tasker.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

@Service
public class UserService {
	private List<User> registeredUsers = new ArrayList<>();
	
	@PostConstruct
	public void init() {
		List<User> users = Arrays.asList( new User[]{
				new User.Builder().name("mickey").password("mouse").id(UUID.randomUUID().toString()).build(),
				new User.Builder().name("minnie").password("mouse").id(UUID.randomUUID().toString()).build(),
				new User.Builder().name("donald").password("duck").id(UUID.randomUUID().toString()).build(),
				new User.Builder().name("daffy").password("duck").id(UUID.randomUUID().toString()).build(),
				new User.Builder().name("mighty").password("mouse").id(UUID.randomUUID().toString()).build(),				
		} );
		
		registeredUsers.addAll(users);
	}
	
	public User getUser(String username, String password) {
		if(username == null || password == null) return null;
		for(User user : registeredUsers) {
			if(user.getName().equals(username) && user.getPassword().equals(password)) return user;
		}
		return null;
	}
	
}
