package com.hunt.tasker.tasks;

import java.util.Date;

public class Task {
	private String description;
	private Date due;
	private String color;
	private Boolean completed;
	private String id;
	private String ownerId;
	
	public String getOwnerId() {
		return ownerId;
	}
	
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
			
	private Task(Builder builder) {
		this.description = builder.description;
		this.due = builder.due;
		this.color = builder.color;
		this.completed = builder.completed;
		this.ownerId = builder.ownerId;
		this.id = builder.id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getDue() {
		return due;
	}
	
	public void setDue(Date due) {
		this.due = due;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public Boolean getCompleted() {
		return completed;
	}
	
	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Task [description=" + description + ", due=" + due + ", color=" + color + ", completed=" + completed + ", id=" + id + ", ownerId=" + ownerId + "]";
	}

	public static class Builder {
		private String description;
		private Date due;
		private String color;
		private Boolean completed;
		private String id;
		private String ownerId;
		
		public Builder() {		
		}
		
		public Builder ownerId(String ownerId) {
			this.ownerId = ownerId;
			return this;
		}
						
		public Builder description(String description) {
			this.description = description;
			return this;
		}
		
		public Builder due(Date due) {
			this.due = due;
			return this;
		}
		
		public Builder color(String color) {
			this.color = color;
			return this;
		}
		
		public Builder completed(Boolean completed) {
			this.completed = completed;
			return this;
		}
		
		public Builder id(String id) {
			this.id = id;
			return this;
		}
		
		public Task build() {
			return new Task(this);
		}
	}
}
