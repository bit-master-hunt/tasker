package com.hunt.tasker.tasks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public class TaskDao {
	// taskId -> Task
	Map<String, Task> tasks = new HashMap<String, Task>();
	
	public Collection<Task> findAll() {
		return tasks.values();
	}
	
	public Task findById(String id) {
		return tasks.get(id);
	}
	
	public List<Task> findByOwner(String ownerId) {
		List<Task> result = new ArrayList<Task>();		
		if(ownerId == null) return result;
		
		for(Task t : tasks.values()) {
			if( ownerId.equals(t.getOwnerId()) ) result.add(t);
		}
		return result;
	}

	public void addTask(Task t) {
		tasks.put(t.getId(), t);
	}
	
	public void delete(Task t) {		
		tasks.remove(t.getId());
	}
	
	public void updateTask(Task t) {
		Task oldTask = findById(t.getId());
		if(oldTask != null) {
			delete(oldTask);
			addTask(t);
		}
	}
}
