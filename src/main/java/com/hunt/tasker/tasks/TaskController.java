package com.hunt.tasker.tasks;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hunt.tasker.users.User;

@Controller
@RequestMapping("/tasks")
@SessionAttributes(value="user", types=User.class)
public class TaskController {
	@Autowired
	private TaskService taskService;
	
	private enum OrderBy { DUE, DESC };
	private enum Order { ASC, DESC };
	
	@ModelAttribute("tasks")
	private List<Task> tasks(User user, 
			@RequestParam(required=false, defaultValue="DESC") OrderBy orderBy,  
			@RequestParam(required=false, defaultValue="ASC") Order order) {		
		List<Task> result = taskService.findAllTasksByUser(user);
		if(orderBy == OrderBy.DESC) {
			Collections.sort(result, new Comparator<Task>() {
				public int compare(Task t1, Task t2) {
					return t1.getDescription().compareToIgnoreCase(t2.getDescription());
				}				
			});
		} else {
			Collections.sort(result, new Comparator<Task>() {
				public int compare(Task t1, Task t2) {
					return t1.getDue().compareTo(t2.getDue());
				}
			});
		}
		
		if(order == Order.DESC) {
			Collections.reverse(result);
		}

		return result;
	}
	
		
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public String createTask(
			@ModelAttribute User user,
			@RequestParam(required=true) String description,
			@RequestParam(required=true, value="due") @DateTimeFormat(iso=ISO.DATE) Date due,
			@RequestParam(required=false, defaultValue="#ffffff", value="color") String color,
			@RequestParam(required=false, defaultValue="false") Boolean completed			
			) {
		
		Task newTask = new Task.Builder()
			.description(description)
			.due(due)
			.color(color)
			.completed(completed)
			.ownerId(user.getId())		
			.build();
				
		taskService.addTask(user, newTask);		
		return "redirect:/tasks";
	}

	@RequestMapping(value = "/delete", method=RequestMethod.GET)
	public String deleteTask(
			@ModelAttribute("user") User user,
			@RequestParam("tid") String taskId
			) {
		Task t = taskService.findTaskById(user, taskId);
		taskService.deleteTask(user, t);
		return "redirect:/tasks";
	}

	@RequestMapping(value = "/{taskId}", method = RequestMethod.POST)
	public String updateTask(@ModelAttribute("user") User user, @PathVariable String taskId) {
		taskService.updateTask(user, taskService.findTaskById(user, taskId));
		return "tasks";
	}

	@RequestMapping(value="", method=RequestMethod.GET)
	public String getTasks(@ModelAttribute User user) {
		if(user == null) return "redirect:/login";
		return "tasks";
	}
}
