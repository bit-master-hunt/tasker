package com.hunt.tasker.tasks;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hunt.tasker.users.User;

@Service
public class TaskService {
	@Autowired
	private TaskDao database;

	private boolean checkOwner(User user, Task t) {
		return user != null && t != null && user.getId() != null && user.getId().equals(t.getOwnerId());
	}

	public List<Task> findAllTasksByUser(User user) {
		return database.findByOwner(user.getId());
	}

	public void addTask(User owner, Task newTask) {
		if (checkOwner(owner, newTask)) {
			newTask.setId(UUID.randomUUID().toString());
			database.addTask(newTask);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void deleteTask(User owner, Task t) {
		if (checkOwner(owner, t)) {
			database.delete(t);
		} else {
			throw new IllegalArgumentException("task " + t + " is not owned by " + owner);
		}
	}

	public void updateTask(User owner, Task t) {
		if(checkOwner(owner, t)) {
			deleteTask(owner, t);
			addTask(owner, t);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public Task findTaskById(User owner, String taskId) {
		Task t = database.findById(taskId);
		if(t.getOwnerId() != null && t.getOwnerId().equals(owner.getId())) {
			return t;
		} else {
			return null;
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Task t: database.findAll()) {
			sb.append(t);
			sb.append("\n");
		}
		return sb.toString();
	}
}
