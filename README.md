### What is this repository for? ###

* Demonstrates how to use Spring MVC to develop a web application.  It emphasizes Controllers, Services, and Repositories.  The repositories are toys.

### How do I get set up? ###

* From the left-hand menu, select the "download" link and then download the zip file.  Unzip this file and then import into Eclipse.